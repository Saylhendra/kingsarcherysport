$(function () {

  "use strict";

  var $image = $(".cropper"),
      $dataX = $("#dataX"),
      $dataY = $("#dataY"),
      $dataHeight = $("#dataHeight"),
      $dataWidth = $("#dataWidth"),
      console = window.console || { log: function () {} },
      cropper;

  
  
  $image.cropper({
    aspectRatio: NaN,
    // autoCropArea: 1,
    data: {
      x: 110,
      y: 60,
      width: 460,
      height: 360
    },
    preview: ".preview",

    // multiple: true,
    // autoCrop: false,
    // dragCrop: false,
    // dashed: false,
    // modal: false,
    // movable: false,
    // resizable: false,
    // zoomable: false,
    // rotatable: false,
    // checkImageOrigin: false,

    // maxWidth: 480,
    // maxHeight: 270,
    // minWidth: 160,
    // minHeight: 90,

    done: function (data) {
      $dataX.val(data.x);
      $dataY.val(data.y);
      $dataHeight.val(data.height);
      $dataWidth.val(data.width);
    },

   build: function (e) {
       console.log(e.type);
       var height=$("#dataHeight").val();
      var width=$("#dataWidth").val(); 
       var dataURLasli = $image.cropper("getDataURL", {
           width: width,
           height:height
       });
       $("#inputImageCrop").text(dataURLasli);
    },

    built: function (e) {
      console.log(e.type);
        var height=$("#dataHeight").val();
      var width=$("#dataWidth").val();
        var dataURLasli = $image.cropper("getDataURL", {
            width: width,
            height:height
        });
        $("#inputImageCropProduct").text(dataURLasli);
    },

    dragstart: function (e) {
      console.log(e.type);
    },

    dragmove: function (e) {
      console.log(e.type);
    },

    dragend: function (e) {
      console.log(e.type);
     var height=$("#dataHeight").val();
      var width=$("#dataWidth").val();
  
         var dataURLasli = $image.cropper("getDataURL", {
            width: width,
            height:height
        });
        $("#inputImageCropProduct").text(dataURLasli);
    }
  });

  cropper = $image.data("cropper");

  

  


  var $inputImage = $("#inputImageProduct"),
      blobURL;

  if (window.URL) {
    $inputImage.change(function () {
      var files = this.files,
          file;

      if (files && files.length) {
        file = files[0];

        if (/^image\/\w+$/.test(file.type)) {
          if (blobURL) {
            URL.revokeObjectURL(blobURL); // Revoke the old one
          }

          blobURL = URL.createObjectURL(file);
          $image.cropper("reset", true).cropper("replace", blobURL);
          $inputImage.val("");
        }
      }
    });
  } else {
    $inputImage.parent().remove();
  }



});

