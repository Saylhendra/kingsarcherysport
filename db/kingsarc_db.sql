drop table if exists barang_images;
drop table if exists kategori;
drop table if exists vendor;
drop table if exists barang;

/*==============================================================*/
/* Table: barang                                                */
/*==============================================================*/
create table barang
(
   id                   varchar(60) not null,
   id_kategori          varchar(60),
   id_vendor            varchar(60),
   nama                 varchar(200),
   deskripsi            text,
   harga                float,
   jumlah               int,
   status               varchar(20),
   created_at           datetime,
   updated_at           datetime,
   primary key (id)
);

/*==============================================================*/
/* Table: barang_images                                         */
/*==============================================================*/
create table barang_images
(
   id                   varchar(60) not null,
   id_barang            varchar(60),
   path_original        varchar(255),
   path_large           text,
   path_medium          mediumtext,
   path_small           mediumtext,
   path_thumbnails      text,
   created_at           datetime,
   updated_at           datetime,
   status               varchar(20) default '0',
   primary key (id)
);

/*==============================================================*/
/* Table: kategori                                              */
/*==============================================================*/
create table kategori
(
   id                   varchar(60) not null,
   nama                 varchar(200),
   created_at           datetime,
   updated_at           datetime,
   primary key (id)
);

/*==============================================================*/
/* Table: vendor                                                */
/*==============================================================*/
create table vendor
(
   id                   varchar(60) not null,
   id_kategori          varchar(60),
   nama                 varchar(200),
   created_at           datetime,
   updated_at           datetime,
   primary key (id)
);