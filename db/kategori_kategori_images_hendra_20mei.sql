/*==============================================================*/
/* Table: kegiatan                                              */
/*==============================================================*/
create table kegiatan
(
   id                   varchar(60) not null,
   kategori_kegiatan    varchar(20) comment 'Kompetisi"
            "Mingguan"
            "Pekanan"
            "Harian',
   nama                 varchar(200),
   lokasi               text,
   deskripsi            text,
   status               varchar(20),
   created_at           datetime,
   updated_at           datetime,
   primary key (id)
);

/*==============================================================*/
/* Table: kegiatan_images                                       */
/*==============================================================*/
create table kegiatan_images
(
   id                   varchar(60) not null,
   id_kegiatan          varchar(60),
   path_original        varchar(255),
   path_large           text,
   path_medium          mediumtext,
   path_small           mediumtext,
   path_thumbnails      text,
   created_at           datetime,
   updated_at           datetime,
   status               varchar(20) default '0',
   primary key (id)
);