<?php
/**
 * Created by PhpStorm.
 * User: GHOZAL
 * Date: 5/19/2016
 * Time: 10:17 AM
 */

$app = getMicroApp();
global $request;
$request = getRequestApp();

$app->get('/api/get_vendor', function () {
    global $request;
    $id = $request->getQuery('id');
    $ids = $request->getQuery('ids');
    $id_kategori = $request->getQuery('id_kategori');
    $id_kategoris = $request->getQuery('id_kategoris');

    $conditions = "1=1 ";
    $bind = array();
    if ( isset($id) ) {
        $conditions .= " and id = :id:";
        $bind[ 'id' ] = $id;
    }
    if ( isset($ids) ) {
        $conditions .= " and id in (" . $ids . ")";
        $bind[ 'ids' ] = $ids;
    }
    if ( isset($id_regency) ) {
        $conditions .= " and id_kategori = :id_kategori:";
        $bind[ 'id_kategori' ] = $id_kategori;
    }
    if ( isset($id_regencies) ) {
        $conditions .= " and id_kategori in (" . $id_kategoris . ")";
        $bind[ 'id_kategoris' ] = $id_kategoris;
    }
    echo json_encode(Vendor::find(array(
        "conditions" => $conditions,
        "bind" => $bind
    ))->toArray());
});