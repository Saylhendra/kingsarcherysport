<?php
$app = getMicroApp();
global $request;
$request = getRequestApp();

/**
 * Display `OK` message
 *
 * @return json string `OK`
 */
$app->get('/api', function () {
    header('HTTP/1.0 200 OK');
    header('Content-Type: application/json');
    $data = array("message" => T::message("api.message.info.httpok"));

    echo json_encode($data);
});

$app->post('/api/login', function () {

    global $request;

    $username = $request->getPost('username');
    $password = $request->getPost('password');
    $device_id = $request->getPost('device_id');
    $os_type = $request->getPost('os_type');
    $app_key = $request->getPost('app_key');

    $result = \MemberService::signinMemberApp($username, $password, $device_id, $os_type, $app_key);
    //$resultNewAkun = \MemberService::signinMemberApp($username, $password, $device_id, $os_type, $app_key);
    header('Content-Type: application/json');
    if ($result->isSuccess === FALSE) {
        header('HTTP/1.0 401 Authorization Required');
    }

    echo json_encode($result);
});

$app->post('/api/logout', function () {
    global $request;
    $session_key = $request->getPost('session_key');
    $id_member = $request->getPost('id_member');
    $app_key = $request->getPost('app_key');

    $result = MemberService::logoutMemberApp($session_key, $id_member, $app_key);
    header('Content-Type: application/json');
    if ($result->isSuccess === FALSE) {
        header('HTTP/1.0 401 Authorization Required');
    }

    echo json_encode($result);
});
