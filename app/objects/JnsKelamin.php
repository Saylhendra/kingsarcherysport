<?php
/**
 * Created by PhpStorm.
 * User: bitsolution
 * Date: 4/9/2015
 * Time: 4:41 PM
 */

class JnsKelamin
{
	//column type news
    public static $IKHWAN = 1;
    public static $AKHWAT = 0;
    public static function getStatus($type){
        //$type = intval($type."");
        switch($type){
            case self::$AKHWAT  : return "AKHWAT (P)";
            default : return "IKHWAN (L)";
        }
    }
}