<?php
function getMicroApp()
{
    return $GLOBALS[ 'MICRO_APP' ];
}

function getRequestApp(){

    if ( !isset($GLOBALS[ 'REQUEST_APP' ]) ) {
        $GLOBALS[ 'REQUEST_APP' ] = new Phalcon\Http\Request();
    }
    return $GLOBALS[ 'REQUEST_APP' ];
}

$app = $GLOBALS[ 'MICRO_APP' ] = new Phalcon\Mvc\Micro();

//
//tambahin disini
include __DIR__ . "/../micro/login.php";
include __DIR__ . "/../micro/master_location.php";
include __DIR__ . "/../micro/combobox.php";

$app->before(function () use ( $app ) {
    $loader = new \Phalcon\Loader();
    $loader->registerDirs(array(
            $GLOBALS[ 'APPLICATION' ][ 'controllersDir' ],
            $GLOBALS[ 'APPLICATION' ][ 'modelsDir' ],
            $GLOBALS[ 'APPLICATION' ][ 'libraryDir' ],
            $GLOBALS[ 'APPLICATION' ][ 'servicesDir' ],
            $GLOBALS[ 'APPLICATION' ][ 'objectsDir' ]
        ));
    $loader->register();
    ob_end_clean();
});

$app->notFound(function () use ( $app ) {
    // not compatible with current config
//    $app->response->setStatusCode(404, "Not Found");
//    $app->response->setContentType('application/json');
//    $app->response->setJsonContent(array(
//        'status' => 'Error 404 - Not Found',
//        'message' => 'Either class or data your are looking for is not found on our server.'
//    ));
//    $app->response->send();
});

$app->finish(function () use ( $app ) {
    exit();
});

$app->handle();