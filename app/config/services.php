<?php

use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Mvc\Model\Manager as ModelsManager;
use Phalcon\Mvc\Collection\Manager as CollectionManager;
/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */
$di = new FactoryDefault();

$di->set('router', function () {
    return require __DIR__ . '/routes.php';
}, TRUE);
/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->set('url', function () use ( $config ) {
    $url = new UrlResolver();
    $url->setBaseUri(base_url());

    return $url;
}, TRUE);

$di->set('crypt', function () {

    $crypt = new Phalcon\Crypt();

    //Set a global encryption key
    $crypt->setKey('%31.1e$i86e$f!8jz');

    return $crypt;
}, TRUE);

/**
 * Setting up the view component
 */
$di->set('view', function () use ( $config ) {

    $view = new View();

    $view->setViewsDir($config->application->viewsDir);

    $view->registerEngines(array(
        '.volt' => function ( $view, $di ) use ( $config ) {

            $volt = new VoltEngine($view, $di);

            $volt->setOptions(array(
                'compiledPath' => $config->application->cacheDir,
                'compiledSeparator' => '_'
            ));

            return $volt;
        },
        '.phtml' => 'Phalcon\Mvc\View\Engine\Php'
    ));

    return $view;
}, TRUE);

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('db', function () use ( $config ) {
    $eventsManager = new \Phalcon\Events\Manager();

    //Listen all the database events
    $eventsManager->attach('db', function ( $event, $connection ) {
        if ( $event->getType() == 'beforeQuery' ) {
            // echo $connection->getSQLStatement();
            if ( $GLOBALS[ 'APPLICATION' ][ "debugSql" ] ) {
                LoggerLibrary::logDebug($connection->getSQLStatement(), "query.log");
            }
        }
    });
    //return new DbAdapter(array(
    $connection = new DbAdapter(array(
        'host' => $config->database->host,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname' => $config->database->dbname
    ));
    $connection->setEventsManager($eventsManager);

    return $connection;
});

/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->set('modelsMetadata', function () {
    return new MetaDataAdapter();
});

/**
 * Start the session the first time some component request the session service
 */
$di->setShared('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});

$di->set('languagesDir', function () use ( $config ) {
    return $config->application->languagesDir;
}, TRUE);

//Set up the flash session service as flash
$di->set('flash', function () {
    return new Phalcon\Flash\Session();
});

$di->set('collectionManager', function() {

    $eventsManager = new EventsManager();

    // Attach an anonymous function as a listener for "model" events
    $eventsManager->attach('collection', function($event, $model) {
        return true;
    });

    // Setting a default EventsManager
    $modelsManager = new CollectionManager();
    $modelsManager->setEventsManager($eventsManager);
    return $modelsManager;

}, true);

$di->setShared('modelsManager', function () {

    $eventsManager = new \Phalcon\Events\Manager();

    // Attach an anonymous function as a listener for "model" events
    $eventsManager->attach('model', function ($event, $model) {
        return true;
    });

    // Setting a default EventsManager
    $modelsManager = new ModelsManager();
    $modelsManager->setEventsManager($eventsManager);

    return $modelsManager;
});
/*
$di->set('mongo', function() {
    $mongo = new MongoClient();
    return $mongo->selectDB("test");
}, true);
*/
Phalcon\Mvc\Model::setup(['exceptionOnFailedSave' => TRUE]);