<?php

$router = new Phalcon\Mvc\Router(FALSE);

$router->add('/:controller/:action/:params', array(
    //'namespace' => 'MyApp\Controllers',
    'controller' => 1,
    'action' => 2,
    'params' => 3,
));

$router->add('/:controller', array(
    //'namespace' => 'MyApp\Controllers',
    'controller' => 1
));
/*
$router->add('/api/:controller/:action/:params', array(
	'namespace' => 'ApplicationApi',
	'controller' => 1,
	'action' => 2,
	'params' => 3,
));

$router->add('/api/:controller', array(
	'namespace' => 'ApplicationApi',
	'controller' => 1
));
*/

$router->add('/admin/:controller/:action/:params', array(
    'namespace' => 'AdminModul',
    'controller' => 1,
    'action' => 2,
    'params' => 3,
));
$router->add('/admin/:controller/:action', array(
    'namespace' => 'AdminModul',
    'controller' => 1,
    'action' => 2,
));

$router->add('/admin/:controller', array(
    'namespace' => 'AdminModul',
    'controller' => 1
));

/*/////////////////////////////////////////*/
$router->add('/quran/:controller/:action/:params', array(
    'namespace' => 'QuranModul',
    'controller' => 1,
    'action' => 2,
    'params' => 3,
));
$router->add('/quran/:controller/:action', array(
    'namespace' => 'QuranModul',
    'controller' => 1,
    'action' => 2,
));

$router->add('/quran/:controller', array(
    'namespace' => 'QuranModul',
    'controller' => 1
));
/*/////////////////////////////////////////*/
$router->add('/front/:controller/:action/:params', array(
    'namespace' => 'FrontModul',
    'controller' => 1,
    'action' => 2,
    'params' => 3,
));
$router->add('/front/:controller/:action', array(
    'namespace' => 'FrontModul',
    'controller' => 1,
    'action' => 2,
));

$router->add('/front/:controller', array(
    'namespace' => 'FrontModul',
    'controller' => 1
));

return $router;