<?php
namespace AdminModul;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class StrDataKucingController extends \ControllerBase
{

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for str_data_kucing
     */
    public function searchAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "StrDataKucing", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id";

        $str_data_kucing = StrDataKucing::find($parameters);
        if (count($str_data_kucing) == 0) {
            $this->flash->notice("The search did not find any str_data_kucing");

            return $this->dispatcher->forward(array(
                "controller" => "str_data_kucing",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $str_data_kucing,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displayes the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a str_data_kucing
     *
     * @param string $id
     */
    public function editAction($id)
    {

        if (!$this->request->isPost()) {

            $str_data_kucing = StrDataKucing::findFirstByid($id);
            if (!$str_data_kucing) {
                $this->flash->error("str_data_kucing was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "str_data_kucing",
                    "action" => "index"
                ));
            }

            $this->view->id = $str_data_kucing->id;

            $this->tag->setDefault("id", $str_data_kucing->id);
            $this->tag->setDefault("id_user", $str_data_kucing->id_user);
            $this->tag->setDefault("nama", $str_data_kucing->nama);
            $this->tag->setDefault("jns_kucing", $str_data_kucing->jns_kucing);
            $this->tag->setDefault("jns_kelamin", $str_data_kucing->jns_kelamin);
            $this->tag->setDefault("jumlah", $str_data_kucing->jumlah);
            $this->tag->setDefault("deskripsi", $str_data_kucing->deskripsi);
            $this->tag->setDefault("date_created", $str_data_kucing->date_created);
            $this->tag->setDefault("date_updated", $str_data_kucing->date_updated);
            
        }
    }

    /**
     * Creates a new str_data_kucing
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "str_data_kucing",
                "action" => "index"
            ));
        }

        $str_data_kucing = new StrDataKucing();

        $str_data_kucing->id = $this->request->getPost("id");
        $str_data_kucing->id_user = $this->request->getPost("id_user");
        $str_data_kucing->nama = $this->request->getPost("nama");
        $str_data_kucing->jns_kucing = $this->request->getPost("jns_kucing");
        $str_data_kucing->jns_kelamin = $this->request->getPost("jns_kelamin");
        $str_data_kucing->jumlah = $this->request->getPost("jumlah");
        $str_data_kucing->deskripsi = $this->request->getPost("deskripsi");
        $str_data_kucing->date_created = $this->request->getPost("date_created");
        $str_data_kucing->date_updated = $this->request->getPost("date_updated");
        

        if (!$str_data_kucing->save()) {
            foreach ($str_data_kucing->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "str_data_kucing",
                "action" => "new"
            ));
        }

        $this->flash->success("str_data_kucing was created successfully");

        return $this->dispatcher->forward(array(
            "controller" => "str_data_kucing",
            "action" => "index"
        ));

    }

    /**
     * Saves a str_data_kucing edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "str_data_kucing",
                "action" => "index"
            ));
        }

        $id = $this->request->getPost("id");

        $str_data_kucing = StrDataKucing::findFirstByid($id);
        if (!$str_data_kucing) {
            $this->flash->error("str_data_kucing does not exist " . $id);

            return $this->dispatcher->forward(array(
                "controller" => "str_data_kucing",
                "action" => "index"
            ));
        }

        $str_data_kucing->id = $this->request->getPost("id");
        $str_data_kucing->id_user = $this->request->getPost("id_user");
        $str_data_kucing->nama = $this->request->getPost("nama");
        $str_data_kucing->jns_kucing = $this->request->getPost("jns_kucing");
        $str_data_kucing->jns_kelamin = $this->request->getPost("jns_kelamin");
        $str_data_kucing->jumlah = $this->request->getPost("jumlah");
        $str_data_kucing->deskripsi = $this->request->getPost("deskripsi");
        $str_data_kucing->date_created = $this->request->getPost("date_created");
        $str_data_kucing->date_updated = $this->request->getPost("date_updated");
        

        if (!$str_data_kucing->save()) {

            foreach ($str_data_kucing->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "str_data_kucing",
                "action" => "edit",
                "params" => array($str_data_kucing->id)
            ));
        }

        $this->flash->success("str_data_kucing was updated successfully");

        return $this->dispatcher->forward(array(
            "controller" => "str_data_kucing",
            "action" => "index"
        ));

    }

    /**
     * Deletes a str_data_kucing
     *
     * @param string $id
     */
    public function deleteAction($id)
    {

        $str_data_kucing = StrDataKucing::findFirstByid($id);
        if (!$str_data_kucing) {
            $this->flash->error("str_data_kucing was not found");

            return $this->dispatcher->forward(array(
                "controller" => "str_data_kucing",
                "action" => "index"
            ));
        }

        if (!$str_data_kucing->delete()) {

            foreach ($str_data_kucing->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "str_data_kucing",
                "action" => "search"
            ));
        }

        $this->flash->success("str_data_kucing was deleted successfully");

        return $this->dispatcher->forward(array(
            "controller" => "str_data_kucing",
            "action" => "index"
        ));
    }

}
