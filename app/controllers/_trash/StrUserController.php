<?php
namespace AdminModul;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class StrUserController extends \ControllerBase
{

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for str_user
     */
    public function searchAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "StrUser", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id";

        $str_user = StrUser::find($parameters);
        if (count($str_user) == 0) {
            $this->flash->notice("The search did not find any str_user");

            return $this->dispatcher->forward(array(
                "controller" => "str_user",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $str_user,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displayes the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a str_user
     *
     * @param string $id
     */
    public function editAction($id)
    {

        if (!$this->request->isPost()) {

            $str_user = StrUser::findFirstByid($id);
            if (!$str_user) {
                $this->flash->error("str_user was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "str_user",
                    "action" => "index"
                ));
            }

            $this->view->id = $str_user->id;

            $this->tag->setDefault("id", $str_user->id);
            $this->tag->setDefault("full_name", $str_user->full_name);
            $this->tag->setDefault("date_created", $str_user->date_created);
            $this->tag->setDefault("date_update", $str_user->date_update);
            
        }
    }

    /**
     * Creates a new str_user
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "str_user",
                "action" => "index"
            ));
        }

        $str_user = new StrUser();

        $str_user->id = $this->request->getPost("id");
        $str_user->full_name = $this->request->getPost("full_name");
        $str_user->date_created = $this->request->getPost("date_created");
        $str_user->date_update = $this->request->getPost("date_update");
        

        if (!$str_user->save()) {
            foreach ($str_user->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "str_user",
                "action" => "new"
            ));
        }

        $this->flash->success("str_user was created successfully");

        return $this->dispatcher->forward(array(
            "controller" => "str_user",
            "action" => "index"
        ));

    }

    /**
     * Saves a str_user edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "str_user",
                "action" => "index"
            ));
        }

        $id = $this->request->getPost("id");

        $str_user = StrUser::findFirstByid($id);
        if (!$str_user) {
            $this->flash->error("str_user does not exist " . $id);

            return $this->dispatcher->forward(array(
                "controller" => "str_user",
                "action" => "index"
            ));
        }

        $str_user->id = $this->request->getPost("id");
        $str_user->full_name = $this->request->getPost("full_name");
        $str_user->date_created = $this->request->getPost("date_created");
        $str_user->date_update = $this->request->getPost("date_update");
        

        if (!$str_user->save()) {

            foreach ($str_user->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "str_user",
                "action" => "edit",
                "params" => array($str_user->id)
            ));
        }

        $this->flash->success("str_user was updated successfully");

        return $this->dispatcher->forward(array(
            "controller" => "str_user",
            "action" => "index"
        ));

    }

    /**
     * Deletes a str_user
     *
     * @param string $id
     */
    public function deleteAction($id)
    {

        $str_user = StrUser::findFirstByid($id);
        if (!$str_user) {
            $this->flash->error("str_user was not found");

            return $this->dispatcher->forward(array(
                "controller" => "str_user",
                "action" => "index"
            ));
        }

        if (!$str_user->delete()) {

            foreach ($str_user->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "str_user",
                "action" => "search"
            ));
        }

        $this->flash->success("str_user was deleted successfully");

        return $this->dispatcher->forward(array(
            "controller" => "str_user",
            "action" => "index"
        ));
    }

}
