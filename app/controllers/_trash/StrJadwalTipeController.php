<?php
namespace AdminModul;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class StrJadwalTipeController extends \ControllerBase
{

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for str_jadwal_tipe
     */
    public function searchAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "StrJadwalTipe", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id";

        $str_jadwal_tipe = StrJadwalTipe::find($parameters);
        if (count($str_jadwal_tipe) == 0) {
            $this->flash->notice("The search did not find any str_jadwal_tipe");

            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal_tipe",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $str_jadwal_tipe,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displayes the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a str_jadwal_tipe
     *
     * @param string $id
     */
    public function editAction($id)
    {

        if (!$this->request->isPost()) {

            $str_jadwal_tipe = StrJadwalTipe::findFirstByid($id);
            if (!$str_jadwal_tipe) {
                $this->flash->error("str_jadwal_tipe was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "str_jadwal_tipe",
                    "action" => "index"
                ));
            }

            $this->view->id = $str_jadwal_tipe->id;

            $this->tag->setDefault("id", $str_jadwal_tipe->id);
            $this->tag->setDefault("nama", $str_jadwal_tipe->nama);
            $this->tag->setDefault("jml_jantan", $str_jadwal_tipe->jml_jantan);
            $this->tag->setDefault("jml_betina", $str_jadwal_tipe->jml_betina);
            $this->tag->setDefault("date_created", $str_jadwal_tipe->date_created);
            $this->tag->setDefault("date_update", $str_jadwal_tipe->date_update);
            
        }
    }

    /**
     * Creates a new str_jadwal_tipe
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal_tipe",
                "action" => "index"
            ));
        }

        $str_jadwal_tipe = new StrJadwalTipe();

        $str_jadwal_tipe->id = $this->request->getPost("id");
        $str_jadwal_tipe->nama = $this->request->getPost("nama");
        $str_jadwal_tipe->jml_jantan = $this->request->getPost("jml_jantan");
        $str_jadwal_tipe->jml_betina = $this->request->getPost("jml_betina");
        $str_jadwal_tipe->date_created = $this->request->getPost("date_created");
        $str_jadwal_tipe->date_update = $this->request->getPost("date_update");
        

        if (!$str_jadwal_tipe->save()) {
            foreach ($str_jadwal_tipe->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal_tipe",
                "action" => "new"
            ));
        }

        $this->flash->success("str_jadwal_tipe was created successfully");

        return $this->dispatcher->forward(array(
            "controller" => "str_jadwal_tipe",
            "action" => "index"
        ));

    }

    /**
     * Saves a str_jadwal_tipe edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal_tipe",
                "action" => "index"
            ));
        }

        $id = $this->request->getPost("id");

        $str_jadwal_tipe = StrJadwalTipe::findFirstByid($id);
        if (!$str_jadwal_tipe) {
            $this->flash->error("str_jadwal_tipe does not exist " . $id);

            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal_tipe",
                "action" => "index"
            ));
        }

        $str_jadwal_tipe->id = $this->request->getPost("id");
        $str_jadwal_tipe->nama = $this->request->getPost("nama");
        $str_jadwal_tipe->jml_jantan = $this->request->getPost("jml_jantan");
        $str_jadwal_tipe->jml_betina = $this->request->getPost("jml_betina");
        $str_jadwal_tipe->date_created = $this->request->getPost("date_created");
        $str_jadwal_tipe->date_update = $this->request->getPost("date_update");
        

        if (!$str_jadwal_tipe->save()) {

            foreach ($str_jadwal_tipe->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal_tipe",
                "action" => "edit",
                "params" => array($str_jadwal_tipe->id)
            ));
        }

        $this->flash->success("str_jadwal_tipe was updated successfully");

        return $this->dispatcher->forward(array(
            "controller" => "str_jadwal_tipe",
            "action" => "index"
        ));

    }

    /**
     * Deletes a str_jadwal_tipe
     *
     * @param string $id
     */
    public function deleteAction($id)
    {

        $str_jadwal_tipe = StrJadwalTipe::findFirstByid($id);
        if (!$str_jadwal_tipe) {
            $this->flash->error("str_jadwal_tipe was not found");

            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal_tipe",
                "action" => "index"
            ));
        }

        if (!$str_jadwal_tipe->delete()) {

            foreach ($str_jadwal_tipe->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal_tipe",
                "action" => "search"
            ));
        }

        $this->flash->success("str_jadwal_tipe was deleted successfully");

        return $this->dispatcher->forward(array(
            "controller" => "str_jadwal_tipe",
            "action" => "index"
        ));
    }

}
