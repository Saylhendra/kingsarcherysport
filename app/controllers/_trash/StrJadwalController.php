<?php
namespace AdminModul;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class StrJadwalController extends AdminControllerBase{

    private $MODEL = "StrJadwal";
    private $TITLE = "Jadwal Steril";
    private $TITLE_BREAD = "List Jadwal";
    private $URL = "str_jadwal";

    public function indexAction(){

        $model = array();
        $model['title'] = $this->TITLE;
        $model['title_bread'] = $this->TITLE_BREAD;
        $model['url'] = $this->URL;
        $model['url_form'] = '/admin/' . $this->URL . '/new';

        StrJadwalController::loadData($this->URL, $model);
    }

    public function loadData($url, $model){
        $dataArray=\StrJadwal::find()->toArray();
        $this->view->partial('admin/' . $url . '/index',
            array("model"=>$model, "dataArray"=>json_decode(json_encode($dataArray)))
        );
    }

    public function newAction(){
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $model = array();
        $model['id'] = $id;

        $model['title'] = $this->TITLE;
        $model['title_bread'] = "Form";

        $model['url'] = $this->URL;
        $model['url_list'] = '/admin/' . $this->URL . '/index';

        $model['action'] = "save";
        $model['labelSubmit'] = "Simpan";

        $this->view->partial('admin/'.$this->URL.'/form', array("model"=>$model));
    }

    /**
     * Searches for str_jadwal
     */
    public function searchAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "StrJadwal", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id";

        $str_jadwal = StrJadwal::find($parameters);
        if (count($str_jadwal) == 0) {
            $this->flash->notice("The search did not find any str_jadwal");

            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $str_jadwal,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Edits a str_jadwal
     *
     * @param string $id
     */
    public function editAction($id)
    {

        if (!$this->request->isPost()) {

            $str_jadwal = StrJadwal::findFirstByid($id);
            if (!$str_jadwal) {
                $this->flash->error("str_jadwal was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "str_jadwal",
                    "action" => "index"
                ));
            }

            $this->view->id = $str_jadwal->id;

            $this->tag->setDefault("id", $str_jadwal->id);
            $this->tag->setDefault("lokasi", $str_jadwal->lokasi);
            $this->tag->setDefault("alamat", $str_jadwal->alamat);
            $this->tag->setDefault("tgl_laksana", $str_jadwal->tgl_laksana);
            $this->tag->setDefault("str_tgl", $str_jadwal->str_tgl);
            $this->tag->setDefault("str_bulan", $str_jadwal->str_bulan);
            $this->tag->setDefault("str_thn", $str_jadwal->str_thn);
            $this->tag->setDefault("id_jadwal_tipe", $str_jadwal->id_jadwal_tipe);
            $this->tag->setDefault("date_created", $str_jadwal->date_created);
            $this->tag->setDefault("date_update", $str_jadwal->date_update);
            
        }
    }

    /**
     * Creates a new str_jadwal
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal",
                "action" => "index"
            ));
        }

        $str_jadwal = new StrJadwal();

        $str_jadwal->id = $this->request->getPost("id");
        $str_jadwal->lokasi = $this->request->getPost("lokasi");
        $str_jadwal->alamat = $this->request->getPost("alamat");
        $str_jadwal->tgl_laksana = $this->request->getPost("tgl_laksana");
        $str_jadwal->str_tgl = $this->request->getPost("str_tgl");
        $str_jadwal->str_bulan = $this->request->getPost("str_bulan");
        $str_jadwal->str_thn = $this->request->getPost("str_thn");
        $str_jadwal->id_jadwal_tipe = $this->request->getPost("id_jadwal_tipe");
        $str_jadwal->date_created = $this->request->getPost("date_created");
        $str_jadwal->date_update = $this->request->getPost("date_update");
        

        if (!$str_jadwal->save()) {
            foreach ($str_jadwal->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal",
                "action" => "new"
            ));
        }

        $this->flash->success("str_jadwal was created successfully");

        return $this->dispatcher->forward(array(
            "controller" => "str_jadwal",
            "action" => "index"
        ));

    }

    /**
     * Saves a str_jadwal edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal",
                "action" => "index"
            ));
        }

        $id = $this->request->getPost("id");

        $str_jadwal = StrJadwal::findFirstByid($id);
        if (!$str_jadwal) {
            $this->flash->error("str_jadwal does not exist " . $id);

            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal",
                "action" => "index"
            ));
        }

        $str_jadwal->id = $this->request->getPost("id");
        $str_jadwal->lokasi = $this->request->getPost("lokasi");
        $str_jadwal->alamat = $this->request->getPost("alamat");
        $str_jadwal->tgl_laksana = $this->request->getPost("tgl_laksana");
        $str_jadwal->str_tgl = $this->request->getPost("str_tgl");
        $str_jadwal->str_bulan = $this->request->getPost("str_bulan");
        $str_jadwal->str_thn = $this->request->getPost("str_thn");
        $str_jadwal->id_jadwal_tipe = $this->request->getPost("id_jadwal_tipe");
        $str_jadwal->date_created = $this->request->getPost("date_created");
        $str_jadwal->date_update = $this->request->getPost("date_update");
        

        if (!$str_jadwal->save()) {

            foreach ($str_jadwal->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal",
                "action" => "edit",
                "params" => array($str_jadwal->id)
            ));
        }

        $this->flash->success("str_jadwal was updated successfully");

        return $this->dispatcher->forward(array(
            "controller" => "str_jadwal",
            "action" => "index"
        ));

    }

    /**
     * Deletes a str_jadwal
     *
     * @param string $id
     */
    public function deleteAction($id)
    {

        $str_jadwal = StrJadwal::findFirstByid($id);
        if (!$str_jadwal) {
            $this->flash->error("str_jadwal was not found");

            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal",
                "action" => "index"
            ));
        }

        if (!$str_jadwal->delete()) {

            foreach ($str_jadwal->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal",
                "action" => "search"
            ));
        }

        $this->flash->success("str_jadwal was deleted successfully");

        return $this->dispatcher->forward(array(
            "controller" => "str_jadwal",
            "action" => "index"
        ));
    }

}
