<?php
namespace AdminModul;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class StrJadwalShiftController extends \ControllerBase
{

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for str_jadwal_shift
     */
    public function searchAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "StrJadwalShift", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id";

        $str_jadwal_shift = StrJadwalShift::find($parameters);
        if (count($str_jadwal_shift) == 0) {
            $this->flash->notice("The search did not find any str_jadwal_shift");

            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal_shift",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $str_jadwal_shift,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displayes the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a str_jadwal_shift
     *
     * @param string $id
     */
    public function editAction($id)
    {

        if (!$this->request->isPost()) {

            $str_jadwal_shift = StrJadwalShift::findFirstByid($id);
            if (!$str_jadwal_shift) {
                $this->flash->error("str_jadwal_shift was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "str_jadwal_shift",
                    "action" => "index"
                ));
            }

            $this->view->id = $str_jadwal_shift->id;

            $this->tag->setDefault("id", $str_jadwal_shift->id);
            $this->tag->setDefault("nama", $str_jadwal_shift->nama);
            $this->tag->setDefault("mulai", $str_jadwal_shift->mulai);
            $this->tag->setDefault("str_mulai", $str_jadwal_shift->str_mulai);
            $this->tag->setDefault("selesai", $str_jadwal_shift->selesai);
            $this->tag->setDefault("str_selesai", $str_jadwal_shift->str_selesai);
            $this->tag->setDefault("date_created", $str_jadwal_shift->date_created);
            $this->tag->setDefault("date_update", $str_jadwal_shift->date_update);
            
        }
    }

    /**
     * Creates a new str_jadwal_shift
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal_shift",
                "action" => "index"
            ));
        }

        $str_jadwal_shift = new StrJadwalShift();

        $str_jadwal_shift->id = $this->request->getPost("id");
        $str_jadwal_shift->nama = $this->request->getPost("nama");
        $str_jadwal_shift->mulai = $this->request->getPost("mulai");
        $str_jadwal_shift->str_mulai = $this->request->getPost("str_mulai");
        $str_jadwal_shift->selesai = $this->request->getPost("selesai");
        $str_jadwal_shift->str_selesai = $this->request->getPost("str_selesai");
        $str_jadwal_shift->date_created = $this->request->getPost("date_created");
        $str_jadwal_shift->date_update = $this->request->getPost("date_update");
        

        if (!$str_jadwal_shift->save()) {
            foreach ($str_jadwal_shift->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal_shift",
                "action" => "new"
            ));
        }

        $this->flash->success("str_jadwal_shift was created successfully");

        return $this->dispatcher->forward(array(
            "controller" => "str_jadwal_shift",
            "action" => "index"
        ));

    }

    /**
     * Saves a str_jadwal_shift edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal_shift",
                "action" => "index"
            ));
        }

        $id = $this->request->getPost("id");

        $str_jadwal_shift = StrJadwalShift::findFirstByid($id);
        if (!$str_jadwal_shift) {
            $this->flash->error("str_jadwal_shift does not exist " . $id);

            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal_shift",
                "action" => "index"
            ));
        }

        $str_jadwal_shift->id = $this->request->getPost("id");
        $str_jadwal_shift->nama = $this->request->getPost("nama");
        $str_jadwal_shift->mulai = $this->request->getPost("mulai");
        $str_jadwal_shift->str_mulai = $this->request->getPost("str_mulai");
        $str_jadwal_shift->selesai = $this->request->getPost("selesai");
        $str_jadwal_shift->str_selesai = $this->request->getPost("str_selesai");
        $str_jadwal_shift->date_created = $this->request->getPost("date_created");
        $str_jadwal_shift->date_update = $this->request->getPost("date_update");
        

        if (!$str_jadwal_shift->save()) {

            foreach ($str_jadwal_shift->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal_shift",
                "action" => "edit",
                "params" => array($str_jadwal_shift->id)
            ));
        }

        $this->flash->success("str_jadwal_shift was updated successfully");

        return $this->dispatcher->forward(array(
            "controller" => "str_jadwal_shift",
            "action" => "index"
        ));

    }

    /**
     * Deletes a str_jadwal_shift
     *
     * @param string $id
     */
    public function deleteAction($id)
    {

        $str_jadwal_shift = StrJadwalShift::findFirstByid($id);
        if (!$str_jadwal_shift) {
            $this->flash->error("str_jadwal_shift was not found");

            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal_shift",
                "action" => "index"
            ));
        }

        if (!$str_jadwal_shift->delete()) {

            foreach ($str_jadwal_shift->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal_shift",
                "action" => "search"
            ));
        }

        $this->flash->success("str_jadwal_shift was deleted successfully");

        return $this->dispatcher->forward(array(
            "controller" => "str_jadwal_shift",
            "action" => "index"
        ));
    }

}
