<?php
namespace AdminModul;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class StrJadwalDetilController extends \ControllerBase
{

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for str_jadwal_detil
     */
    public function searchAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "StrJadwalDetil", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id";

        $str_jadwal_detil = StrJadwalDetil::find($parameters);
        if (count($str_jadwal_detil) == 0) {
            $this->flash->notice("The search did not find any str_jadwal_detil");

            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal_detil",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $str_jadwal_detil,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displayes the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a str_jadwal_detil
     *
     * @param string $id
     */
    public function editAction($id)
    {

        if (!$this->request->isPost()) {

            $str_jadwal_detil = StrJadwalDetil::findFirstByid($id);
            if (!$str_jadwal_detil) {
                $this->flash->error("str_jadwal_detil was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "str_jadwal_detil",
                    "action" => "index"
                ));
            }

            $this->view->id = $str_jadwal_detil->id;

            $this->tag->setDefault("id", $str_jadwal_detil->id);
            $this->tag->setDefault("id_jadwal", $str_jadwal_detil->id_jadwal);
            $this->tag->setDefault("id_jadwal_shift", $str_jadwal_detil->id_jadwal_shift);
            $this->tag->setDefault("betina_kuota", $str_jadwal_detil->betina_kuota);
            $this->tag->setDefault("betina_sisa_kuota", $str_jadwal_detil->betina_sisa_kuota);
            $this->tag->setDefault("jantan_kuota", $str_jadwal_detil->jantan_kuota);
            $this->tag->setDefault("jantan_sisa_kuota", $str_jadwal_detil->jantan_sisa_kuota);
            $this->tag->setDefault("tot_kuota", $str_jadwal_detil->tot_kuota);
            $this->tag->setDefault("tot_sisa", $str_jadwal_detil->tot_sisa);
            $this->tag->setDefault("date_created", $str_jadwal_detil->date_created);
            $this->tag->setDefault("date_update", $str_jadwal_detil->date_update);
            
        }
    }

    /**
     * Creates a new str_jadwal_detil
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal_detil",
                "action" => "index"
            ));
        }

        $str_jadwal_detil = new StrJadwalDetil();

        $str_jadwal_detil->id = $this->request->getPost("id");
        $str_jadwal_detil->id_jadwal = $this->request->getPost("id_jadwal");
        $str_jadwal_detil->id_jadwal_shift = $this->request->getPost("id_jadwal_shift");
        $str_jadwal_detil->betina_kuota = $this->request->getPost("betina_kuota");
        $str_jadwal_detil->betina_sisa_kuota = $this->request->getPost("betina_sisa_kuota");
        $str_jadwal_detil->jantan_kuota = $this->request->getPost("jantan_kuota");
        $str_jadwal_detil->jantan_sisa_kuota = $this->request->getPost("jantan_sisa_kuota");
        $str_jadwal_detil->tot_kuota = $this->request->getPost("tot_kuota");
        $str_jadwal_detil->tot_sisa = $this->request->getPost("tot_sisa");
        $str_jadwal_detil->date_created = $this->request->getPost("date_created");
        $str_jadwal_detil->date_update = $this->request->getPost("date_update");
        

        if (!$str_jadwal_detil->save()) {
            foreach ($str_jadwal_detil->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal_detil",
                "action" => "new"
            ));
        }

        $this->flash->success("str_jadwal_detil was created successfully");

        return $this->dispatcher->forward(array(
            "controller" => "str_jadwal_detil",
            "action" => "index"
        ));

    }

    /**
     * Saves a str_jadwal_detil edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal_detil",
                "action" => "index"
            ));
        }

        $id = $this->request->getPost("id");

        $str_jadwal_detil = StrJadwalDetil::findFirstByid($id);
        if (!$str_jadwal_detil) {
            $this->flash->error("str_jadwal_detil does not exist " . $id);

            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal_detil",
                "action" => "index"
            ));
        }

        $str_jadwal_detil->id = $this->request->getPost("id");
        $str_jadwal_detil->id_jadwal = $this->request->getPost("id_jadwal");
        $str_jadwal_detil->id_jadwal_shift = $this->request->getPost("id_jadwal_shift");
        $str_jadwal_detil->betina_kuota = $this->request->getPost("betina_kuota");
        $str_jadwal_detil->betina_sisa_kuota = $this->request->getPost("betina_sisa_kuota");
        $str_jadwal_detil->jantan_kuota = $this->request->getPost("jantan_kuota");
        $str_jadwal_detil->jantan_sisa_kuota = $this->request->getPost("jantan_sisa_kuota");
        $str_jadwal_detil->tot_kuota = $this->request->getPost("tot_kuota");
        $str_jadwal_detil->tot_sisa = $this->request->getPost("tot_sisa");
        $str_jadwal_detil->date_created = $this->request->getPost("date_created");
        $str_jadwal_detil->date_update = $this->request->getPost("date_update");
        

        if (!$str_jadwal_detil->save()) {

            foreach ($str_jadwal_detil->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal_detil",
                "action" => "edit",
                "params" => array($str_jadwal_detil->id)
            ));
        }

        $this->flash->success("str_jadwal_detil was updated successfully");

        return $this->dispatcher->forward(array(
            "controller" => "str_jadwal_detil",
            "action" => "index"
        ));

    }

    /**
     * Deletes a str_jadwal_detil
     *
     * @param string $id
     */
    public function deleteAction($id)
    {

        $str_jadwal_detil = StrJadwalDetil::findFirstByid($id);
        if (!$str_jadwal_detil) {
            $this->flash->error("str_jadwal_detil was not found");

            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal_detil",
                "action" => "index"
            ));
        }

        if (!$str_jadwal_detil->delete()) {

            foreach ($str_jadwal_detil->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "str_jadwal_detil",
                "action" => "search"
            ));
        }

        $this->flash->success("str_jadwal_detil was deleted successfully");

        return $this->dispatcher->forward(array(
            "controller" => "str_jadwal_detil",
            "action" => "index"
        ));
    }

}
