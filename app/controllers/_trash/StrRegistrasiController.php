<?php
namespace AdminModul;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class StrRegistrasiController extends \ControllerBase
{

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for str_registrasi
     */
    public function searchAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "StrRegistrasi", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id";

        $str_registrasi = StrRegistrasi::find($parameters);
        if (count($str_registrasi) == 0) {
            $this->flash->notice("The search did not find any str_registrasi");

            return $this->dispatcher->forward(array(
                "controller" => "str_registrasi",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $str_registrasi,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displayes the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a str_registrasi
     *
     * @param string $id
     */
    public function editAction($id)
    {

        if (!$this->request->isPost()) {

            $str_registrasi = StrRegistrasi::findFirstByid($id);
            if (!$str_registrasi) {
                $this->flash->error("str_registrasi was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "str_registrasi",
                    "action" => "index"
                ));
            }

            $this->view->id = $str_registrasi->id;

            $this->tag->setDefault("id", $str_registrasi->id);
            $this->tag->setDefault("id_acc", $str_registrasi->id_acc);
            $this->tag->setDefault("id_jadwal", $str_registrasi->id_jadwal);
            $this->tag->setDefault("no_reg", $str_registrasi->no_reg);
            $this->tag->setDefault("qrcode", $str_registrasi->qrcode);
            $this->tag->setDefault("sts_bayar", $str_registrasi->sts_bayar);
            $this->tag->setDefault("sts_aktif", $str_registrasi->sts_aktif);
            $this->tag->setDefault("date_created", $str_registrasi->date_created);
            $this->tag->setDefault("date_update", $str_registrasi->date_update);
            
        }
    }

    /**
     * Creates a new str_registrasi
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "str_registrasi",
                "action" => "index"
            ));
        }

        $str_registrasi = new StrRegistrasi();

        $str_registrasi->id = $this->request->getPost("id");
        $str_registrasi->id_acc = $this->request->getPost("id_acc");
        $str_registrasi->id_jadwal = $this->request->getPost("id_jadwal");
        $str_registrasi->no_reg = $this->request->getPost("no_reg");
        $str_registrasi->qrcode = $this->request->getPost("qrcode");
        $str_registrasi->sts_bayar = $this->request->getPost("sts_bayar");
        $str_registrasi->sts_aktif = $this->request->getPost("sts_aktif");
        $str_registrasi->date_created = $this->request->getPost("date_created");
        $str_registrasi->date_update = $this->request->getPost("date_update");
        

        if (!$str_registrasi->save()) {
            foreach ($str_registrasi->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "str_registrasi",
                "action" => "new"
            ));
        }

        $this->flash->success("str_registrasi was created successfully");

        return $this->dispatcher->forward(array(
            "controller" => "str_registrasi",
            "action" => "index"
        ));

    }

    /**
     * Saves a str_registrasi edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "str_registrasi",
                "action" => "index"
            ));
        }

        $id = $this->request->getPost("id");

        $str_registrasi = StrRegistrasi::findFirstByid($id);
        if (!$str_registrasi) {
            $this->flash->error("str_registrasi does not exist " . $id);

            return $this->dispatcher->forward(array(
                "controller" => "str_registrasi",
                "action" => "index"
            ));
        }

        $str_registrasi->id = $this->request->getPost("id");
        $str_registrasi->id_acc = $this->request->getPost("id_acc");
        $str_registrasi->id_jadwal = $this->request->getPost("id_jadwal");
        $str_registrasi->no_reg = $this->request->getPost("no_reg");
        $str_registrasi->qrcode = $this->request->getPost("qrcode");
        $str_registrasi->sts_bayar = $this->request->getPost("sts_bayar");
        $str_registrasi->sts_aktif = $this->request->getPost("sts_aktif");
        $str_registrasi->date_created = $this->request->getPost("date_created");
        $str_registrasi->date_update = $this->request->getPost("date_update");
        

        if (!$str_registrasi->save()) {

            foreach ($str_registrasi->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "str_registrasi",
                "action" => "edit",
                "params" => array($str_registrasi->id)
            ));
        }

        $this->flash->success("str_registrasi was updated successfully");

        return $this->dispatcher->forward(array(
            "controller" => "str_registrasi",
            "action" => "index"
        ));

    }

    /**
     * Deletes a str_registrasi
     *
     * @param string $id
     */
    public function deleteAction($id)
    {

        $str_registrasi = StrRegistrasi::findFirstByid($id);
        if (!$str_registrasi) {
            $this->flash->error("str_registrasi was not found");

            return $this->dispatcher->forward(array(
                "controller" => "str_registrasi",
                "action" => "index"
            ));
        }

        if (!$str_registrasi->delete()) {

            foreach ($str_registrasi->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "str_registrasi",
                "action" => "search"
            ));
        }

        $this->flash->success("str_registrasi was deleted successfully");

        return $this->dispatcher->forward(array(
            "controller" => "str_registrasi",
            "action" => "index"
        ));
    }

}
