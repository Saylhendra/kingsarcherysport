<?php
use Phalcon\Mvc\Controller;

class ControllerBase extends Controller{

    protected $isSuccess = TRUE;
    protected $message;
    protected $data;

    public function init(){
        //JsLibrary::add_js("index/script/login.box.small");
        //JsLibrary::add_js("index/script/chainedLob");
    }

    public function generateRandomString( $length = 10 ){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[ rand(0, strlen($characters) - 1) ];
        }

        return $randomString;
    }

    public function uuidString(){
        //sleep(1);
        return $this->generateRandomString(8) . uniqid($this->generateRandomString(4)) . uniqid(uniqid() . "") . $this->generateRandomString(5);
    }

    public function uuid(){
        list($usec, $sec) = explode(" ", microtime());

        return str_replace(".", "", ((float)$usec + (float)$sec) . "");
    }

    public function getResponse(){
        return array(
            "isSuccess" => $this->isSuccess,
            "message" => $this->message,
            "data" => $this->data
        );
    }

    function toUnderscore( $input ){
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[ 0 ];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
    }

}
