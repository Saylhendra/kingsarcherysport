<?php
/**
 * Created by PhpStorm.
 * User: GHOZAL
 * Date: 5/19/2016
 * Time: 11:01 AM
 */

namespace AdminModul;
use Phalcon\Http\Client\Exception;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class KegiatanImagesController extends \ControllerBase
{
    private $URL = "kegiatan_images";
    private $kegiatanName ="";

    public function indexAction(){
        self::loadAction();
//        $model = array();
//        $model['url'] = $this->URL;
//        ProdukKategoriController::loadData($this->URL, $model);
    }

    public function loadData($url, $model){
        $dataArray = array();
        $this->view->partial('admin/' . $url . '/index',
            array("model"=>$model, "dataArray"=>json_decode(json_encode($dataArray)))
        );
    }

    public function loadAction(){
        $id = $_GET['id'];
        $kegiatan = \Kegiatan::findFirstById($id);
        $this->kegiatanName = $kegiatan?$kegiatan->nama."<i class='fa fa-angle-right'></i>":'';
        $model = array();
        $model['pageName'] = "<a href='".base_url()."/admin/kegiatan/index'>Activity</a>
<i class='fa fa-angle-right'></i>".$this->kegiatanName." Activity Images Master";
        $model['url'] = $this->URL;
        $model['idKegiatan'] = $id;
        $this->view->partial('admin/'.$this->URL.'/index', $model);
    }

    public function getAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $data = \KegiatanImages::findFirstById($id);
        if($data)
            echo json_encode($data->toArray());
        else
            echo json_encode(array());
    }

    public function listAction(){
        $idKegiatan = $_GET['idKegiatan'];
        $start = isset($_GET['start'])?$_GET['start']:0;
        $length = isset($_GET['length'])?$_GET['length']:0;
        $draw = isset($_GET['draw'])?$_GET['draw']:0;
        $search = isset($_GET['search'])?$_GET['search']['value']:"";

        $conditions = "id_kegiatan=:id_kegiatan: ORDER BY updated_at DESC";
        $bind = array("id_kegiatan"=>$idKegiatan);

        $listData = \KegiatanImages::find(array(
            "conditions"=>$conditions,
            "limit"=>$length,
            "offset"=>$start,
            "bind"=>$bind
        ));
        $list = array();
//        foreach($listData as $val){
//            $temp = $val->toArray();
//            $temp['nama_kategori'] = $val->kategori?$val->kategori->nama:"";
//            $list[] = $temp;
//        }

        $count = \KegiatanImages::count(array(
            "conditions"=>$conditions,
            "bind"=>$bind
        ));
        $total = $count;//\KegiatanImages::count();

        $results = array(
            "draw" => $draw,
            "recordsTotal" => $total,
            "recordsFiltered" => $count,
            "data"=>$listData->toArray()
        );
        echo json_encode($results);
    }

    public function newAction(){
        $id = isset($_GET['id']) ? $_GET['id'] : "0";
        $idKegiatan = $id;
        $model = array();
        $model['id'] = $id;
        $model['pageName'] = "Activity Images Form";
        $model['url'] = $this->URL;
        $model['action'] = "save";
        $model['labelSubmit'] = "Save";
        $model['labelForm'] = "Create";
        $model['idKegiatan'] = $idKegiatan;
        $this->view->partial('admin/'.$this->URL.'/form', $model);
    }

    public function editAction()
    {

        $id = isset($_GET['id'])?$_GET['id']:"0";
        $idKegiatan = \KegiatanImages::findFirstById($id);
        $model = array();
        $model['id'] = $id;
        $model['pageName'] = "Activity Images Form";
        $model['url'] = $this->URL;
        $model['action'] = "update";
        $model['labelSubmit'] = "Update";
        $model['labelForm'] = "Edit";
        $model['idKegiatan'] = $idKegiatan->id_kegiatan;
        $this->view->partial('admin/'.$this->URL.'/form',$model);
    }

    public function saveAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            if (isset($_FILES['detailfile'])) {
                $file_image = $_FILES['detailfile'];
            } else {
                $file_image = null;
            }
            $upload = \UploadLibrary::upload_picture($file_image);
            if ($upload['isSuccess']) {
                $data = new \KegiatanImages();
                $data->id_kegiatan = $_POST['id_kegiatan'];
                $data->id = $this->uuidString();
//                $data->path_original = $upload['path_original'];
                $data->path_large = $upload['path_large'];
                $data->path_medium = $upload['path_medium'];
                $data->path_small = $upload['path_small'];
                $data->path_thumbnails = $upload['path_thumbnails'];
                $data->created_at = date("Y-m-d H:i:s");
                $data->updated_at = date("Y-m-d H:i:s");
                $data->save();
                $response->message = \T::message("all.label.message.success_save");

            }
            $this->db->commit();
        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function updateAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $id = $this->request->getPost("id");
            $data = \KegiatanImages::findFirstById($id);
            $data->assign($_POST);
            $data->updated_at = date("Y-m-d H:i:s");
            $data->update();
            $response->message = \T::message("all.label.message.success_save");
            $this->db->commit();

        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function removeAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $id = $this->request->getPost("id");
            $data = \KegiatanImages::findFirstById($id);
            $data->delete();
            $response->message = \T::message("all.label.message.success_delete");
            $this->db->commit();
        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }
}