<?php
namespace AdminModul;

class AdminControllerBase extends \ControllerBase{

    public function beforeExecuteRoute($dispatcher){
        $isLoginAdmin = $this->session->isLoginAdmin;

        // This is executed before every found action
        if (!$isLoginAdmin) {

            $this->response->redirect('/login/admin?message=Silahkan login terlebih dahulu!');
            $this->view->disable();

            return false;
        }
    }

    public function afterExecuteRoute($dispatcher)
    {
        // Executed after every found action
    }

}
