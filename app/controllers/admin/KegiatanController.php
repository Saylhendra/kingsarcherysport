<?php
namespace AdminModul;
use Phalcon\Http\Client\Exception;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class KegiatanController extends \ControllerBase{

    private $URL = "kegiatan";

    public function indexAction(){
        self::loadAction();
//        $model = array();
//        $model['url'] = $this->URL;
//        ProdukKategoriController::loadData($this->URL, $model);
    }

    public function loadData($url, $model){
        $dataArray = array();
        $this->view->partial('admin/' . $url . '/index',
            array("model"=>$model, "dataArray"=>json_decode(json_encode($dataArray)))
        );
    }

    public function loadAction(){
        $model = array();
        $model['pageName'] = "Activity Master";
        $model['url'] = $this->URL;
        $this->view->partial('admin/'.$this->URL.'/index', $model);
    }

    public function getAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $data = \Kegiatan::findFirstById($id);
        if($data)
            echo json_encode($data->toArray());
        else
            echo json_encode(array());
    }

    public function listAction(){
        $start = isset($_GET['start'])?$_GET['start']:0;
        $length = isset($_GET['length'])?$_GET['length']:0;
        $draw = isset($_GET['draw'])?$_GET['draw']:0;
        $search = isset($_GET['search'])?$_GET['search']['value']:"";

        $conditions = "nama like :search: ORDER BY updated_at DESC";
        $bind = array("search"=>"%".$search."%");

        $listData = \Kegiatan::find(array(
            "conditions"=>$conditions,
            "limit"=>$length,
            "offset"=>$start,
            "bind"=>$bind
        ));
//        $list = array();
//        foreach($listData as $val){
//            $temp = $val->toArray();
//            $temp['nama_kategori'] = $val->kategori?$val->kategori->nama:"";
//            $temp['nama_vendor'] = $val->vendor?$val->vendor->nama:"";
//            $list[] = $temp;
//        }

        $count = \Kegiatan::count(array(
            "conditions"=>$conditions,
            "bind"=>$bind
        ));
        $total = \Kegiatan::count();

        $results = array(
            "draw" => $draw,
            "recordsTotal" => $total,
            "recordsFiltered" => $count,
            "data"=>$listData->toArray()
        );
        echo json_encode($results);
    }

    public function newAction(){
        $id = isset($_GET['id']) ? $_GET['id'] : "0";
        $model = array();
        $model['id'] = $id;
        $model['pageName'] = "Activity Form";
        $model['url'] = $this->URL;
        $model['action'] = "save";
        $model['labelSubmit'] = "Save";
        $model['labelForm'] = "Create";
        $this->view->partial('admin/'.$this->URL.'/form', $model);
    }

    public function editAction()
    {

        $id = isset($_GET['id'])?$_GET['id']:"0";
        $model = array();
        $model['id'] = $id;
        $model['pageName'] = "Activity Form";
        $model['url'] = $this->URL;
        $model['action'] = "update";
        $model['labelSubmit'] = "Update";
        $model['labelForm'] = "Edit";
        $this->view->partial('admin/'.$this->URL.'/form',$model);
    }

    public function saveAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $data = new \Kegiatan();
            $data->assign($_POST);
            $data->id = $this->uuidString();
            $data->created_at = date("Y-m-d H:i:s");
            $data->updated_at = date("Y-m-d H:i:s");
            $data->save();
            $response->message = \T::message("all.label.message.success_save");
            $this->db->commit();
        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function updateAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $id = $this->request->getPost("id");
            $data = \Kegiatan::findFirstById($id);
            $data->assign($_POST);
            $data->updated_at = date("Y-m-d H:i:s");
            $data->update();
            $response->message = \T::message("all.label.message.success_save");
            $this->db->commit();

        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function removeAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $id = $this->request->getPost("id");
            $data = \Kegiatan::findFirstById($id);
            $data->delete();
            $img = \KegiatanImages::findByIdKegiatan($id);
            $img->delete();
            $response->message = \T::message("all.label.message.success_delete");
            $this->db->commit();
        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }
}