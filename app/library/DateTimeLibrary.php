<?php
use Phalcon\Logger\Adapter\File as FileAdapter;

class DateTimeLibrary
{

    public static $FORMAT_DATETIME_IO = "d/m/Y H:i:s";
    public static $FORMAT_DATE_IO = "d/m/Y";

    public static $FORMAT_DATETIME_SQL = "Y-m-d H:i:s";
    public static $FORMAT_DATE_SQL = "Y-m-d";

    public static function parseDateTimeIOtoSQL($dt)
    {
        $date = \DateTime::createFromFormat(DateTimeLibrary::$FORMAT_DATETIME_IO, $dt);
        return $date->format(DateTimeLibrary::$FORMAT_DATETIME_SQL);
    }

    public static function parseDateIOtoSQL($dt)
    {
        $oldDate = $dt; //15-09-2015
        $arr = explode('/', $oldDate);
        $newDate = $arr[2].'-'.$arr[1].'-'.$arr[0];

        $date = \DateTime::createFromFormat(DateTimeLibrary::$FORMAT_DATE_IO, $dt);
        return $newDate;
    }

    public static function parseDateTimeSQLtoIO($dt)
    {
        $oldDate = $dt; //15-09-2015
        $arr = explode(' ', $oldDate);
        $oldDateExp = explode('/', $arr[0]);
        $newDate = $oldDateExp[2].'-'.$oldDateExp[1].'-'.$oldDateExp[0];
        $newTime = $arr[1];
        $newDateTime = $newDate.' '.$newTime;

        $date = \DateTime::createFromFormat(DateTimeLibrary::$FORMAT_DATETIME_SQL, $dt);
        return $newDateTime;
    }

    public static function parseDateSQLtoIO($dt)
    {
        $oldDate = $dt; //2015-09-15-09
        $arr = explode('-', $oldDate);
        $newDate = $arr[2].'/'.$arr[1].'/'.$arr[0];
        $date = \DateTime::createFromFormat(DateTimeLibrary::$FORMAT_DATE_SQL, $dt);
        return $date->format(DateTimeLibrary::$FORMAT_DATE_IO);
    }

    public static function now()
    {
        return date(DateTimeLibrary::$FORMAT_DATETIME_SQL);
    }

	public static function get_date_time($mysqldate)
	{
        $phpdate = strtotime( $mysqldate );
        return date( 'm/d/Y H:i:s', $phpdate );
	}
    public static function add_days_by_date($n,$date,$format = "Y-m-d H:i:s")
    {
        $date = strtotime($date);
        $date = strtotime("+".$n." day", $date);
        return date($format,$date);
    }
    public static function add_days_by_date_from_now($n,$format = "Y-m-d H:i:s")
    {
        $date = date("Y-m-d H:i:s");
        $date = strtotime($date);
        $date = strtotime("+".$n." day", $date);
        return date($format,$date);
    }

}