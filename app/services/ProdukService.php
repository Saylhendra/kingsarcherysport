<?php

class ProdukService extends ServiceBase{

    public static function saveProdukService($dataPost, $fileImage){
        $response = new ResponseObject();
        try{
            $data = new \SqBinaan();
            $data->assign($dataPost);
            $data->id = generateUuidString();
            if (array_key_exists('fileImage', $fileImage)) {
                $uploadimg = \UploadLibrary::upload_picture($fileImage['fileImage']);
                if ($uploadimg['isSuccess']) {
                    $data->path_small = $uploadimg['path_small'];
                    $data->path_medium = $uploadimg['path_medium'];
                    $data->path_large = $uploadimg['path_large'];
                    $data->path_thumbnails = $uploadimg['path_thumbnails'];
                }
            }
            /*=========================================*/
            $data->created_date = date('Y-m-d H:i:s');
            $data->update_date = date('Y-m-d H:i:s');
            //$data->save();
            $response->isSuccess = true;
            $response->message = "Success";

        }catch(Exception $ex){
            $response->isSuccess = false;
            $response->message = $ex->getTraceAsString();
        }
        return $response;
    }

}