<?php

class Barang extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $id;

    /**
     *
     * @var string
     */
    public $id_kategori;

    /**
     *
     * @var string
     */
    public $id_vendor;

    /**
     *
     * @var string
     */
    public $nama;

    /**
     *
     * @var string
     */
    public $deskripsi;

    /**
     *
     * @var double
     */
    public $harga;

    /**
     *
     * @var integer
     */
    public $jumlah;

    /**
     *
     * @var string
     */
    public $status;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    public function initialize()
    {
        $this->belongsTo('id_kategori', 'Kategori', 'id', NULL);
        $this->belongsTo('id_vendor', 'Vendor', 'id', NULL);
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'id_kategori' => 'id_kategori', 
            'id_vendor' => 'id_vendor', 
            'nama' => 'nama', 
            'deskripsi' => 'deskripsi', 
            'harga' => 'harga', 
            'jumlah' => 'jumlah', 
            'status' => 'status', 
            'created_at' => 'created_at', 
            'updated_at' => 'updated_at'
        );
    }

}
