<?php

class StrRegistrasi extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $id;

    /**
     *
     * @var string
     */
    public $id_acc;

    /**
     *
     * @var string
     */
    public $id_jadwal;

    /**
     *
     * @var string
     */
    public $no_reg;

    /**
     *
     * @var string
     */
    public $qrcode;

    /**
     *
     * @var integer
     */
    public $sts_bayar;

    /**
     *
     * @var integer
     */
    public $sts_aktif;

    /**
     *
     * @var string
     */
    public $date_created;

    /**
     *
     * @var string
     */
    public $date_update;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'id_acc' => 'id_acc', 
            'id_jadwal' => 'id_jadwal', 
            'no_reg' => 'no_reg', 
            'qrcode' => 'qrcode', 
            'sts_bayar' => 'sts_bayar', 
            'sts_aktif' => 'sts_aktif', 
            'date_created' => 'date_created', 
            'date_update' => 'date_update'
        );
    }

}
