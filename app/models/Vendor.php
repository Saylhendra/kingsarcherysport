<?php

class Vendor extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $id;

    /**
     *
     * @var string
     */
    public $id_kategori;

    /**
     *
     * @var string
     */
    public $nama;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    public function initialize()
    {
        $this->belongsTo('id_kategori', 'Kategori', 'id', NULL);
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'id_kategori' => 'id_kategori', 
            'nama' => 'nama', 
            'created_at' => 'created_at', 
            'updated_at' => 'updated_at'
        );
    }

}
