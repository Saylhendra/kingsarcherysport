<?php

class Kegiatan extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $id;

    /**
     *
     * @var string
     */
    public $kategori_kegiatan;

    /**
     *
     * @var string
     */
    public $nama;

    /**
     *
     * @var string
     */
    public $lokasi;

    /**
     *
     * @var string
     */
    public $deskripsi;

    /**
     *
     * @var string
     */
    public $status;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'kategori_kegiatan' => 'kategori_kegiatan', 
            'nama' => 'nama', 
            'lokasi' => 'lokasi', 
            'deskripsi' => 'deskripsi', 
            'status' => 'status', 
            'created_at' => 'created_at', 
            'updated_at' => 'updated_at'
        );
    }

}
