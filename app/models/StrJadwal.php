<?php

class StrJadwal extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $id;

    /**
     *
     * @var string
     */
    public $id_jadwal_tipe;

    /**
     *
     * @var string
     */
    public $lokasi;

    /**
     *
     * @var string
     */
    public $alamat;

    /**
     *
     * @var string
     */
    public $tgl_laksana;

    /**
     *
     * @var string
     */
    public $str_tgl;

    /**
     *
     * @var string
     */
    public $str_bulan;

    /**
     *
     * @var string
     */
    public $str_thn;

    /**
     *
     * @var integer
     */
    public $sts_aktif;

    /**
     *
     * @var integer
     */
    public $bts_tempo_bayar;

    /**
     *
     * @var string
     */
    public $date_created;

    /**
     *
     * @var string
     */
    public $date_update;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'id_jadwal_tipe' => 'id_jadwal_tipe', 
            'lokasi' => 'lokasi', 
            'alamat' => 'alamat', 
            'tgl_laksana' => 'tgl_laksana', 
            'str_tgl' => 'str_tgl', 
            'str_bulan' => 'str_bulan', 
            'str_thn' => 'str_thn', 
            'sts_aktif' => 'sts_aktif', 
            'bts_tempo_bayar' => 'bts_tempo_bayar', 
            'date_created' => 'date_created', 
            'date_update' => 'date_update'
        );
    }

}
