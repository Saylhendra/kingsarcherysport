<?php

class StrJadwalShift extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $id;

    /**
     *
     * @var string
     */
    public $nama;

    /**
     *
     * @var string
     */
    public $jm_mulai;

    /**
     *
     * @var string
     */
    public $str_mulai;

    /**
     *
     * @var string
     */
    public $jm_selesai;

    /**
     *
     * @var string
     */
    public $str_selesai;

    /**
     *
     * @var string
     */
    public $date_created;

    /**
     *
     * @var string
     */
    public $date_update;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'nama' => 'nama', 
            'jm_mulai' => 'jm_mulai', 
            'str_mulai' => 'str_mulai', 
            'jm_selesai' => 'jm_selesai', 
            'str_selesai' => 'str_selesai', 
            'date_created' => 'date_created', 
            'date_update' => 'date_update'
        );
    }

}
