<?php

class StrAccount extends \Phalcon\Mvc\Model
{
    public $id;
    public $id_role;
    public $id_user;
    public $username;
    public $password;
    public $sts_aktif;
    public $date_created;
    public $date_updated;

    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'id_role' => 'id_role', 
            'id_user' => 'id_user', 
            'username' => 'username', 
            'password' => 'password', 
            'sts_aktif' => 'sts_aktif', 
            'date_created' => 'date_created', 
            'date_updated' => 'date_updated'
        );
    }

    public function initialize()
    {
        $this->belongsTo('id_role', 'StrRole', 'id', array("alias"=>"str_role"));
        $this->belongsTo('id_user', 'StrUser', 'id', array("alias"=>"str_user"));
    }

}
