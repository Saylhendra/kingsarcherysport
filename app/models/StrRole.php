<?php

class StrRole extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $id;

    /**
     *
     * @var string
     */
    public $nama;

    /**
     *
     * @var string
     */
    public $kode_role;

    /**
     *
     * @var string
     */
    public $date_created;

    /**
     *
     * @var string
     */
    public $date_updated;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'nama' => 'nama', 
            'kode_role' => 'kode_role', 
            'date_created' => 'date_created', 
            'date_updated' => 'date_updated'
        );
    }

}
