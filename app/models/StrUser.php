<?php

class StrUser extends \Phalcon\Mvc\Model
{
    public $id;
    public $nama;
    public $email;
    public $jns_kelamin;
    public $tempat_lahir;
    public $tgl_lahir;
    public $alamat;
    public $tlp;
    public $date_created;
    public $date_updated;

    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'nama' => 'nama', 
            'email' => 'email',
            'jns_kelamin' => 'jns_kelamin',
            'tempat_lahir' => 'tempat_lahir', 
            'tgl_lahir' => 'tgl_lahir', 
            'alamat' => 'alamat', 
            'tlp' => 'tlp', 
            'date_created' => 'date_created', 
            'date_updated' => 'date_updated'
        );
    }

}
