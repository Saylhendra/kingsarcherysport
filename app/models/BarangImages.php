<?php

class BarangImages extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $id;

    /**
     *
     * @var string
     */
    public $id_barang;

    /**
     *
     * @var string
     */
    public $path_original;

    /**
     *
     * @var string
     */
    public $path_large;

    /**
     *
     * @var string
     */
    public $path_medium;

    /**
     *
     * @var string
     */
    public $path_small;

    /**
     *
     * @var string
     */
    public $path_thumbnails;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     *
     * @var string
     */
    public $status;

    public function initialize()
    {
        $this->belongsTo('id_barang', 'BarangImages', 'id', NULL);
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'id_barang' => 'id_barang', 
            'path_original' => 'path_original', 
            'path_large' => 'path_large', 
            'path_medium' => 'path_medium', 
            'path_small' => 'path_small', 
            'path_thumbnails' => 'path_thumbnails', 
            'created_at' => 'created_at', 
            'updated_at' => 'updated_at', 
            'status' => 'status'
        );
    }

}
