<?php

class StrJadwalTipe extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $id;

    /**
     *
     * @var string
     */
    public $nama;

    /**
     *
     * @var integer
     */
    public $jml_jantan;

    /**
     *
     * @var integer
     */
    public $jml_betina;

    /**
     *
     * @var string
     */
    public $date_created;

    /**
     *
     * @var string
     */
    public $date_update;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'nama' => 'nama', 
            'jml_jantan' => 'jml_jantan', 
            'jml_betina' => 'jml_betina', 
            'date_created' => 'date_created', 
            'date_update' => 'date_update'
        );
    }

}
